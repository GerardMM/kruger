package com.example.kruger;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AudioActivity extends AppCompatActivity {


    MediaRecorder mediaRecorder;
    Button button;
    @BindView(R.id.btnStop)
    Button btnStop;
    @BindView(R.id.btnRecord)
    Button btnRecord;

    //Firebase
    private FirebaseStorage storage = FirebaseStorage.getInstance("gs://krugerdatabase.appspot.com");
    private StorageReference storageRef = storage.getReference();
    private StorageReference newAudio;
    private Uri filePath;

    //Camera
    private String currentAudioPath;

    public Uri getFilePath() {
        return filePath;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        ButterKnife.bind(this);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PackageManager.PERMISSION_GRANTED);

        mediaRecorder = new MediaRecorder();
    }
    
    @RequiresApi(api = Build.VERSION_CODES.O)
    @OnClick(R.id.btnRecord)
    public void onRecordClicked(View view) {
        startRecording(view);
    }


    @OnClick(R.id.btnStop)
    public void onStopClicked(View view) {
        stopRecording(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startRecording(View view) {
        try {
            Toast.makeText(getApplicationContext(), "Grabando", Toast.LENGTH_SHORT).show();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "3GP_" + timeStamp + "_";
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File file = File.createTempFile(imageFileName,".3gp", path);
            filePath = Uri.fromFile(file);

            currentAudioPath = file.getAbsolutePath();
            mediaRecorder.setOutputFile(file);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            mediaRecorder.prepare();
            mediaRecorder.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopRecording(View view) {
        Toast.makeText(getApplicationContext(), "Parando grabación", Toast.LENGTH_SHORT).show();
        mediaRecorder.stop();
        mediaRecorder.release();
        uploadFile();
        Intent INT = new Intent(AudioActivity.this, MapsActivity.class);
        startActivity(INT);
    }

    private void uploadFile() {
        filePath = Uri.fromFile(new File(currentAudioPath));
        newAudio = storageRef.child("audios/"+filePath.getLastPathSegment());

        Bundle b = getIntent().getExtras();
        double lat = -1;
        double lon = -1;
        if (b != null) {
            lat = b.getDouble("latitude");
            lon = b.getDouble("longitude");
        }
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setCustomMetadata("Lat", ""+ lat)
                .setCustomMetadata("Lon", ""+ lon)
                .build();

        //photoMarker();

        UploadTask uploadTask = newAudio.putFile(filePath,metadata);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            }
        });
    }
}


