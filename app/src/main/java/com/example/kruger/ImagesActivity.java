package com.example.kruger;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kruger.adapter.DataAdapter;
import com.example.kruger.model.AnimalImage;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import java.util.ArrayList;

public class ImagesActivity extends AppCompatActivity {

    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference refList = storage.getReference().child("images");
    private RecyclerView mRecyclerView;
    public ArrayList<AnimalImage> list = new ArrayList<>();
    private DataAdapter dataAdapter = new DataAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        mRecyclerView = findViewById(R.id.recycle);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerView.setAdapter(dataAdapter);

        dataFromFirebase();
    }

    public void dataFromFirebase() {
        refList.listAll().addOnSuccessListener(new OnSuccessListener<ListResult>() {
            @Override
            public void onSuccess(ListResult listResult) {
                for (StorageReference item : listResult.getItems()) {
                    downloadFile(item);


                }
            }
        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    private void downloadFile(StorageReference reference) {
        AnimalImage animal = new AnimalImage();
        reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String url = uri.toString();
                animal.setImage(url);
                reference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(StorageMetadata storageMetadata) {
                        String latitude = storageMetadata.getCustomMetadata("Lat");
                        String longitude = storageMetadata.getCustomMetadata("Lon");
                        String data = storageMetadata.getCustomMetadata("Data");
                        animal.setDate(data);
                        animal.setLat(Double.parseDouble(latitude));
                        animal.setLon(Double.parseDouble(longitude));
                        list.add(animal);
                        onAnimalChanged(list);
                    }
                });
            }
        });
    }

    private void onAnimalChanged(ArrayList<AnimalImage> list) {
        dataAdapter.setList(list);
    }
}
