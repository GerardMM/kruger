package com.example.kruger;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;

import com.example.kruger.model.AnimalImage;
import com.example.kruger.model.AudioSound;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //GoogleMap
    private GoogleMap mMap;

    //Audio permissions
    private static final String RECORD_AUDIO = Manifest.permission.RECORD_AUDIO;
    private static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final int REQUEST_AUDIO_PERMISSION_CODE = 1;

    //Location permissions
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private boolean permissionsGranted = false;

    //Location vars
    private FusedLocationProviderClient mFuseLocationProviderClient;
    private Location currentLocation;
    private static final float DEFAULT_ZOOM = 20f;
    private double currentLatitude;
    private double currentLongitude;

    //Camera
    private String currentPhotoPath;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    //Firebase
    private FirebaseStorage storage = FirebaseStorage.getInstance("gs://krugerdatabase.appspot.com");
    private StorageReference storageRef = storage.getReference();
    private StorageReference newImage;
    private Uri filePath;

    private StorageReference refList = storage.getReference().child("images");

    ArrayList<AnimalImage> listAnimals = new ArrayList<>();
    ArrayList<AudioSound> audioSounds = new ArrayList<>();

    AudioActivity audioActivity = new AudioActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        getLocationPermission();
        getAudioPermission();
        openMap();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uploadFile();
        Intent intent = new Intent(MapsActivity.this, AudioActivity.class);
        Bundle b = new Bundle();
        b.putDouble("latitude", currentLatitude);
        b.putDouble("longitude", currentLongitude);
        intent.putExtras(b);
        startActivity(intent);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        dataFromFirebase();
        LatLng ITB = new LatLng(41.453211, 2.186530);
        Marker marker = mMap.addMarker(new MarkerOptions().position(ITB).title("Marker in ITB"));
        marker.setTag(ITB);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                double lat = marker.getPosition().latitude;
                double lon = marker.getPosition().longitude;
                showImage(lat, lon);
                return false;
            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ITB));
    }

    public void openMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION)==PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this.getApplicationContext(),COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            permissionsGranted = true;
        } else {
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private void getAudioPermission(){
        String[] permissions = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),RECORD_AUDIO)==PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this.getApplicationContext(),WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED){
            permissionsGranted = true;
        } else {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_AUDIO_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsGranted = false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if (grantResults.length>0){
                    for (int i=0;grantResults.length>i;i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            permissionsGranted = false;
                            return;
                        }
                    }
                    permissionsGranted = true;
                }
            }
        }
    }

    @OnClick(R.id.albumButton)
    public void onAlbumButtonClicked(){
        Intent intent = new Intent(MapsActivity.this, ImagesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.locationButton)
    public  void onLocationButtonClicked(){
        if (permissionsGranted){
            getLocation();
        } else {
            getLocationPermission();
        }
    }

    @OnClick(R.id.cameraButton)
    public void onViewClicked() {
        dispatchTakePictureIntent();

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                        "com.exemple.kruger.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

                getLocation();
            }
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        galleryAddPic(currentPhotoPath);
        return image;
    }

    private void galleryAddPic(String filePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getApplicationContext().sendBroadcast(mediaScanIntent);
    }

    private void uploadFile() {
        filePath = Uri.fromFile(new File(currentPhotoPath));
        newImage = storageRef.child("images/"+filePath.getLastPathSegment());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String dateString = format.format(date);

        StorageMetadata metadata = new StorageMetadata.Builder()
                .setCustomMetadata("Lat", ""+currentLatitude)
                .setCustomMetadata("Lon", ""+currentLongitude)
                .setCustomMetadata("Data", ""+ dateString)
                .build();

        UploadTask uploadTask = newImage.putFile(filePath,metadata);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            }
        });
    }

    private void getLocation() {
        mFuseLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (permissionsGranted) {
                Task location = mFuseLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            currentLocation = (Location) task.getResult();
                            currentLatitude = currentLocation.getLatitude();
                            currentLongitude = currentLocation.getLongitude();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(currentLocation.getLatitude(),
                                            currentLocation.getLongitude()), DEFAULT_ZOOM));
                        } else {
                            Toast.makeText(MapsActivity.this, "Unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Log.e("Security Exception: %s", e.getMessage());
        }
    }

    public void showImage(double latitudeMarker, double longitudeMarker) {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        //MediaPlayer player;
        ImageView imageView = new ImageView(this);
        for (int i = 0; i < listAnimals.size(); ++i) {
            if(listAnimals.get(i).getLon() == longitudeMarker && listAnimals.get(i).getLat() == latitudeMarker) {
                Picasso.get().load(listAnimals.get(i).getImage()).into(imageView);
                //player = MediaPlayer.create(getApplicationContext(), Uri.fromFile(new File(audioSounds.get(0).getSound())));
                //player.start();
            }
        }

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }

    private void addAllMarkers(double lat, double lon) {
        Marker marker =  mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
    }

    public void dataFromFirebase() {
        refList.listAll().addOnSuccessListener(new OnSuccessListener<ListResult>() {
            @Override
            public void onSuccess(ListResult listResult) {
                for (StorageReference item : listResult.getItems()) {
                    downloadPhoto(item);
                    downloadAudio(item);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    private void downloadPhoto(StorageReference reference) {
        AnimalImage animal = new AnimalImage();
        reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String url = uri.toString();
                animal.setImage(url);
                reference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(StorageMetadata storageMetadata) {
                        String latitude = storageMetadata.getCustomMetadata("Lat");
                        String longitude = storageMetadata.getCustomMetadata("Lon");
                        String data = storageMetadata.getCustomMetadata("Data");
                        animal.setDate(data);
                        animal.setLat(Double.parseDouble(latitude));
                        animal.setLon(Double.parseDouble(longitude));

                        addAllMarkers(animal.getLat(), animal.getLon());

                        listAnimals.add(animal);
                    }
                });
            }
        });
    }

    private void downloadAudio(StorageReference reference) {
        AudioSound audioSound = new AudioSound();
        reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String url = uri.toString();
                audioSound.setSound(url);
                reference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(StorageMetadata storageMetadata) {
                        String latitude = storageMetadata.getCustomMetadata("Lat");
                        String longitude = storageMetadata.getCustomMetadata("Lon");
                        audioSound.setLat(Double.parseDouble(latitude));
                        audioSound.setLon(Double.parseDouble(longitude));

                        //addAllMarkers(a.getLat(), animal.getLon());

                        audioSounds.add(audioSound);
                    }
                });
            }
        });
    }
}
