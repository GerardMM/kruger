package com.example.kruger.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kruger.R;
import com.squareup.picasso.Picasso;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kruger.model.AnimalImage;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {

    private List<AnimalImage> list = new ArrayList<>();

    public void setList(List<AnimalImage> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        AnimalImage item = list.get(position);
        Picasso.get().load(item.getImage()).into(holder.imageViewer);
        holder.dateView.setText(item.getDate());
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (list != null) size = list.size();
        return size;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageViewer)
        ImageView imageViewer;
        @BindView(R.id.dateView)
        TextView dateView;

        public DataViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
